require 'net/http'
require 'net/http/post/multipart'
require 'uri'
require 'json'
require 'yaml'
require 'nokogiri'

module Hybris
  
  def connect 
    yield Shell.connect(@host, @port)
  end
  
  class Shell
    def initialize(host, port)
      @host = host
      @port = port
      @user = 'admin'
      @password = 'nimda'
    end
    
    def self.connect(host, port)
      hybris = Hybris::Shell.new(host, port)
      if block_given?
        yield hybris
      else
        hybris
      end
    end
    
    def get(path)
      Net::HTTP.start(@host, @port, {:read_timeout => 120 * 60}) do |http|
        req = Net::HTTP::Get.new(path)
        req.basic_auth(@user, @password)
        headers = {'Accept' => 'application/json'}
        response = http.request(req)
        if block_given?
          yield response
        else
          response
        end
      end
    end
    
    def post(path, payload, args = {})
      Net::HTTP.start(@host, @port, {:read_timeout => 120 * 60}) do |http|
        http.continue_timeout = 120 * 60
        req = Net::HTTP::Post.new(path)
        req.basic_auth(@user, @password)
        if args.has_key?(:ct)
          req.add_field('Content-Type', args[:ct])
        end
        req.body = payload
        response = http.request(req)
        if block_given?
          yield response
        else
          response
        end
      end
    end
    
    def post_form(path, form)
      Net::HTTP.start(@host, @port, {:read_timeout => 120 * 60}) do |http|
        http.continue_timeout = 120 * 60
        req = Net::HTTP::Post.new(path)
        req.basic_auth(@user, @password)
        req.form_data = form
        req.add_field('Accept', 'application/json')
        response = http.request(req)
        if block_given?
          yield response
        else
          response
        end
      end
    end
    
    def upload(path, form, files)
      Net::HTTP.start(@host, @port, {:read_timeout => 120 * 60}) do |http|
        http.continue_timeout = 120 * 60
        f = Hash[files.map {|k,v| [k, UploadIO.new(File.new(v), v)]}]
        req = Net::HTTP::Post::Multipart.new(path, form.merge!(f))
        req.basic_auth(@user, @password)
        req.add_field('Accept', 'application/json')
        response = http.request(req)
        if block_given?
          yield response
        else
          response
        end
      end
    end
    
    def status
      get '/platform/init/data' do |resp|
        if (resp.code.to_i < 400)
          resp = JSON.parse(resp.body);
          if (resp['isInitializing'])
            "Init"
          else
            "Working"
          end
        else
          "error"
        end
      end
    end
    
    def working?
      status == "Working"
    end
    
    def setLogging(category, level)
      payload = URI.encode_www_form({:loggerName => category, :levelName => level})
      post_form '/platform/log4j/changeLevel', {:loggerName => category, :levelName => level} do |resp|
        puts resp.body
      end
    end
    
    def off(category)
      setLogging(category, "OFF")
    end
    
    def info(category)
      setLogging(category, "INFO")
    end
    
    def load(filename, args = {})
      f = File.new(filename)
      threads = args[:threads] || 1
      params = {:validationEnum => 'IMPORT_STRICT', :maxThreads => threads, :encoding =>'UTF-8', :legacyMode => 'true', '_legacyMode'=>'on', '_enableCodeExecution'=>'on'}
      resp = if f.size < 20000
        content = File.read(filename)
        post_form '/console/impex/import', params.merge!({:scriptContent => content})
      else
        upload '/console/impex/import/upload', params, {:file => filename}
      end
      process_upload resp
    end
    
    def process_upload(resp)
      document = Nokogiri::HTML(resp.body)
      result = document.css('#impexResult')
      if "error" == result.attr('data-level').text
        message = document.css('.impexResult pre').text
        message
      else
        "Ok"
      end
    end
    
    def query(query, args = {})
      max = args[:max] || 20
      locale = args[:locale] || 'en'
      user = args[:user] || @user
      commit = args[:commit] || 'false'
      payload = {:flexibleSearchQuery => query, :maxCount => max, :locale => locale, :user => user, :commit => commit}
      post_form "/console/flexsearch/execute", payload do |resp|
        begin
          h = JSON.parse(resp.body)
          puts h['headers'].join "|"
          h['resultList'].each {|l| puts l.join '|' }
        rescue Exception => e
          error(e, resp)
        end
      end
      ""
    end
    
    def groovy(commands, args = {})
      commit = args[:commit] || false
      payload = {:script => commands, :commit => commit}
      post_form "/console/groovy/execute", payload do |resp|
        begin
          content = JSON.parse(resp.body)
          {:result => content['executionResult'], :output => content['outputText'], :errors => content['stacktraceText']}
        rescue Exception => e
          error(e, resp)
          {:result => 'Not JSON. See trace'}
        end
      end
    end
    
    def gscript(file, args = {})
      commands = File.open(file, 'rb') {|f| f.read}
      groovy(commands, args)
    end
    
    def dryupdate
      dryrun(true)
    end
    
    def dryinit
      dryrun(true)
    end
    
    def dryrun(update)
      payload = if update
        {:update => true, :initialize => false}
      else
        {:update => false, :initialize => true}
      end
      post '/platform/dryrun/execute', payload.to_json, {:ct => 'application/json'} do |response|
        begin
          r = JSON.parse(response.body)
          {
            :drop => r['ddlDropFileName'], 
            :ddl => r['ddlFileName'],
            :dml => r['dmlFileName'], 
            :path => r['path']
          }
        rescue Exception => e
          error(e, response)
        end
      end
    end
    
    def download(url, dest)
       f = File.new(dest, 'wb')
       begin
         get url do |resp|
           f.write(resp.body)
         end
       ensure
         f.close
       end
    end
    
    def error(e, resp)
      puts e
      puts "Response is not in expected form. Response code #{resp.code}"
      puts resp.body
    end
    
    def get_binding
      return binding()
    end
  end
end