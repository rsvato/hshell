#! /usr/bin/env ruby -Ilib
require 'hybris'
include Hybris

script = File.open(ARGV[2]) { |f| f.read }
@host = ARGV[0]
@port = ARGV[1]
connect do |hybris|
  hybris.instance_eval(script)
end